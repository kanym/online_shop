from django.db import models


class Contact(models.Model):
    adress = models.CharField(max_length=200)
    contact = models.CharField(max_length=100)
    email = models.EmailField()

    
    def __str__(self):
        return self.contact