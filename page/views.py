from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from shop.models import Product, Category
from page.models import Contact

class HomePageView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'page/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories']= Category.objects.all()
        context['contacts']= Contact.objects.all()
        return context
        

class AboutPageView(TemplateView):
    template_name = 'page/about.html'


class ContactPageView(TemplateView):
    template_name = 'page/contact.html'